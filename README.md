Apoyo a las Asesorias de Programacion del CECyT 9
===============================

En este repositorio iremos subiendo la resolución de algunos de los ejercicios Propuestos por el grupo de asesorías de Programación del CECyT 9 como muestra de apoyo. Encontrarás tres carpetas en las que te mostraremos el problema resuelto en Python, JavaScript y Java.
