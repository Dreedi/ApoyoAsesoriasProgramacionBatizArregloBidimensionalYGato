// Aceptamos el Arreglo
console.log("------------------------------------------------------------");
arreglo = [[18,64,27,65],[34,88,94,33],[12,68,43,23]];
console.log("Hola! para JavaScript trabajaremos con un arreglo defninido del siguiente formato: \n");
for(i = 0; i < 3; i++)
{	
	for(j = 0; j < 4; j++)
	{
		process.stdout.write("| " + arreglo[i][j] + " |");
	}
	console.log("");
}
console.log("------------------------------------------------------------\n\n");
// Aceptamos el Arreglo

// Analizando Filas
console.log("------------------------------------------------------------");
console.log("Resultados por fila ...");
sumaFila = [];
filaPorArreglos = [];
for(i = 0; i < 3; i++)
{
	suma = 0;
	filaArreglo = []
	for(j = 0; j < 4; j++)
	{
		filaArreglo.push(arreglo[i][j]);
		suma += arreglo[i][j];
	}
	sumaFila.push(suma);
	filaPorArreglos.push(filaArreglo);
}

for(i = 0; i < 3; i++)
{
	for(j = 0; j < 4;j++)
	{
		if(j==3)
		{
			process.stdout.write(arreglo[i][j]+"");
		}
		else
		{
			process.stdout.write(arreglo[i][j] + " + ");
		}
	}
	process.stdout.write(" = " + sumaFila[i] + " / Mayor Fila = " + Math.max.apply(null, filaPorArreglos[i]) + " / Menor Fila = " + Math.min.apply(null, filaPorArreglos[i]) + "\n");
}
console.log("------------------------------------------------------------\n\n");
// Analizando Filas

// Analizando Columnas
console.log("------------------------------------------------------------");
console.log("Resultados por columna ...");
sumaColumna = [];
columnaPorArreglos = [];
for(i = 0; i < 4; i++)
{
	suma = 0;
	columnaArreglo = []
	for(j = 0; j < 3; j++)
	{
		columnaArreglo.push(arreglo[j][i]);
		suma += arreglo[j][i];
	}
	sumaColumna.push(suma);
	columnaPorArreglos.push(columnaArreglo);
}

for(i = 0; i < 4; i++)
{
	console.log("\nResultados columna: ", (i+1));
	console.log("Suma de los numeros: ", sumaColumna[i]);
	console.log("Mayor de los numeros: ", Math.max.apply(null, columnaPorArreglos[i]));
	console.log("Menor de los numeros: ", Math.min.apply(null, columnaPorArreglos[i]));
}
console.log("------------------------------------------------------------\n\n");
// Analizando Columnas

// Analizando TODO
console.log("------------------------------------------------------------");
console.log("Resultados GENERALES...");
menor = 9999;
mayor = 0;
for(i = 0; i < 3; i++)
{
	for(j = 0; j < 4;j++)
	{
		if(arreglo[i][j] > mayor)
		{
			mayor = arreglo[i][j];
		}
		else if(arreglo[i][j] < menor)
		{
			menor = arreglo[i][j];
		}
	}
}
console.log("\nMayor de todos los numeros: ",mayor);
console.log("\nMenor de todos los numeros: ",menor);
console.log("------------------------------------------------------------");
// Analizando TODO