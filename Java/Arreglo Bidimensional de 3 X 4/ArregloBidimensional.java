import java.util.Scanner;
import java.lang.Math;
public class ArregloBidimensional
{
	public static void main(String[] args)
	{
		Scanner leer = new Scanner(System.in);
		int[][] arreglo = new int[3][4];
		// Aceptando el arreglo
		System.out.println("Hola! Necesito que me ayudes a llenar un arreglo de 3 X 4 de enteros\n\n");	
		System.out.println("------------------------------------------------------------");
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				System.out.println("Dame el numero entero que va en la posicion: " + i + "," + j);
				arreglo[i][j] = leer.nextInt();
			}	
		}
		System.out.println("------------------------------------------------------------\n\n");
		System.out.println("------------------------------------------------------------");
		System.out.println("Me has dado una matriz del siguiente formato");
		for (int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				System.out.print("| " + arreglo[i][j] + " |");
			}
			System.out.println("");
		}
		System.out.println("------------------------------------------------------------\n\n");
		// Aceptando el arreglo

		// Analizando Filas
		System.out.println("------------------------------------------------------------");
		System.out.println("Resultado por filas");
		int[] sumaFila = new int[3];
		int[] menoresFila = new int[3];
		int[] mayoresFila = new int[3];
		for (int i = 0; i < 3; i++)
		{
			int suma = 0;
			int menor = 99999;
			int mayor = 0;
			for (int j = 0; j < 4; j++)
			{
				suma += arreglo[i][j];
				if(arreglo[i][j] > mayor)
				{
					mayor = arreglo[i][j];
					if(arreglo[i][j] < menor)
					{
						menor = arreglo[i][j];
					}
				}
				else if(arreglo[i][j] < menor)
				{
					menor = arreglo[i][j];
				}
			}
			sumaFila[i] = suma;
			menoresFila[i] = menor;
			mayoresFila[i] = mayor;
		}
		for (int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				if(j==3)
				{
					System.out.print(arreglo[i][j]);
				}
				else
				{
					System.out.print(arreglo[i][j] + " + ");
				}
			}
			System.out.print(" = " + sumaFila[i] + " / Mayor Fila = " + mayoresFila[i] + " / Menor Fila = " + menoresFila[i] + "\n");
		}
		System.out.println("------------------------------------------------------------\n\n");
		// Analizando Filas

		// Analizando Columnas
		System.out.println("------------------------------------------------------------");
		System.out.println("Resultados por columnas");
		int[] sumaColumna = new int[4];
		int[] menoresColumna = new int[4];
		int[] mayoresColumna = new int[4];
		for (int i = 0; i < 4; i++)
		{
			int suma = 0;
			int menor = 99999;
			int mayor = 0;
			for (int j = 0; j < 3; j++)
			{
				suma += arreglo[j][i];
				if(arreglo[j][i] > mayor)
				{
					mayor = arreglo[j][i];
					if(arreglo[j][i] < menor)
					{
						menor = arreglo[j][i];
					}
				}
				else if(arreglo[j][i] < menor)
				{
					menor = arreglo[j][i];
				}
			}
			sumaColumna[i] = suma;
			menoresColumna[i] = menor;
			mayoresColumna[i] = mayor;
		}
		for(int i = 0; i < 4; i++)
		{
			System.out.println("\nResultado columna: " + (i+1));
			System.out.println("Suma de los numeros: " + sumaColumna[i]);
			System.out.println("Mayor de los numeros: " + mayoresColumna[i]);
			System.out.println("Menor de los numeros: " + menoresColumna[i]);
		}
		System.out.println("------------------------------------------------------------\n\n");
		// Analizando Columnas

		// Analizando TODO
		System.out.println("------------------------------------------------------------");
		System.out.println("Resultados GENERALES");
		int menor = 99999;
		int mayor = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if(arreglo[i][j] > mayor)
				{
					mayor = arreglo[i][j];
					if(arreglo[i][j] < menor)
					{
						menor = arreglo[i][j];
					}
				}
				else if(arreglo[i][j] < menor)
				{
					menor = arreglo[i][j];
				}
			}
		}
		System.out.println("\nMayor de todos los numeros: " + mayor);
		System.out.println("\nMenor de todos los numeros: " + menor);
		// Analizando TODO
	}
}