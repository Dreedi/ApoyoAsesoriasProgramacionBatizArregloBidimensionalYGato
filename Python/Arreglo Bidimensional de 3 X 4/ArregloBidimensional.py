# Aceptando arreglo
print "Hola! Necesito que me ayudes a llenar un arreglo de 3 X 4 de enteros"
print "\n"*2
print "-" * 60
arreglo = []
for i in range(3):
	arregloPrimerNivel = []
	for j in range(4):
		mensaje = "Dame el numero entero que va en la posicion: "+str(i)+","+str(j)+"\n"
		arregloPrimerNivel.append(int(raw_input(mensaje)))
	arreglo.append(arregloPrimerNivel)
print "-" * 60
print "\n"*2
print "-" * 60
print "Me has dado un arreglo con el siguiente formato:\n", arreglo
print "El cual si lo pintamos en una matriz se representaria asi:\n"
for i in range(3):
	for j in range(4):
		print "| ", str(arreglo[i][j]), " |",
	print ""
print "-" * 60
# Aceptando arreglo

# Analizando Filas
print "\n"*2
print "-" * 60
print "Resultados por fila ..."
sumaFila = []
filaPorArreglos = []
for i in range(3):
	suma = 0
	filaArreglo = []
	for j in range(4):
		filaArreglo.append(arreglo[i][j])
		suma += arreglo[i][j]
	sumaFila.append(suma)
	filaPorArreglos.append(filaArreglo)

for i in range(3):
	for j in range(4):
		if j == 3:
			print str(arreglo[i][j]),
		else:
			print str(arreglo[i][j]), " + ",
	print " = ", sumaFila[i], " / Mayor Fila = ", max(filaPorArreglos[i]), " / Menor Fila = ", min(filaPorArreglos[i])
print "-" * 60
# Analizando Filas

#Analizando Columnas
print "\n"*2
print "-" * 60
print "Resultados por columna ..."
sumaColumna = []
columnaPorArreglos = []
for i in range(4):
	suma = 0
	columnaArreglo = []
	for j in range(3):
		columnaArreglo.append(arreglo[j][i])
		suma += arreglo[j][i]
	sumaColumna.append(suma)
	columnaPorArreglos.append(columnaArreglo)
for i in range(4):
	print "\nResultados columna: ", i+1
	print "Suma de los numeros: ", sumaColumna[i]
	print "Mayor de los numeros: ", max(columnaPorArreglos[i])
	print "Menor de los numeros: ", min(columnaPorArreglos[i])
print "-" * 60
#Analizando Columnas

#Analizando TODO
print "\n"*2
print "-" * 60
print "Resultados GENERALES..."
print "\nMayor de todos los numeros: ", max(max(arreglo))
print "\Menor de todos los numeros: ", min(min(arreglo))
print "-" * 60
#Analizando TODO