# Aceptando arreglo
print "Hola! Necesito que me ayudes a llenar un arreglo de 3 X 4 de enteros"
arreglo = []
sumaFila = []
filaPorArreglos = []
for i in range(3):
	arregloPrimerNivel = []
	suma = 0
	filaArreglo = []
	for j in range(4):
		mensaje = "Dame el numero entero que va en la posicion: "+str(i)+","+str(j)+"\n"
		arregloPrimerNivel.append(int(raw_input(mensaje)))
		suma += arregloPrimerNivel[j]
		filaArreglo.append(arregloPrimerNivel[j])
	sumaFila.append(suma)
	filaPorArreglos.append(filaArreglo)
	arreglo.append(arregloPrimerNivel)

for i in range(3):
	for j in range(4):
		if j == 3:
			print str(arreglo[i][j]),
		else:
			print str(arreglo[i][j]), " + ",
	print " = ", sumaFila[i], " / Mayor Fila = ", max(filaPorArreglos[i]), " / Menor Fila = ", min(filaPorArreglos[i])

sumaColumna = []
columnaPorArreglos = []
for i in range(4):
	suma = 0
	columnaArreglo = []
	for j in range(3):
		columnaArreglo.append(arreglo[j][i])
		suma += arreglo[j][i]
	sumaColumna.append(suma)
	columnaPorArreglos.append(columnaArreglo)
for i in range(4):
	print "\nResultados columna: ", i+1
	print "Suma de los numeros: ", sumaColumna[i]
	print "Mayor de los numeros: ", max(columnaPorArreglos[i])
	print "Menor de los numeros: ", min(columnaPorArreglos[i])
print "-" * 60
#Analizando Columnas

#Analizando TODO
print "\n"*2
print "-" * 60
print "Resultados GENERALES..."
print "\nMayor de todos los numeros: ", max(max(arreglo))
print "\Menor de todos los numeros: ", min(min(arreglo))
print "-" * 60
#Analizando TODO