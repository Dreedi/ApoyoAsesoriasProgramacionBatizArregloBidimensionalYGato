import random, time, sys
print "Bienvenido al juego de Gato, estas jugando contra la maquina. Para tirar 'U' en una casilla tienes que teclear su numero \n"

numeros = [1,2,3,4,5,6,7,8,9]
arreglo = [[1,2,3],[4,5,6],[7,8,9]]

def jugar(arreglo):
	print "Tu inicias!"
	ganado = analizarJugada(arreglo)
	movimientos = 0
	turnoU = True
	pintarGato(arreglo)
	while not ganado:
		if turnoU:
			if movimientos >= 9:
				analizarJugada(arreglo)
				print "Empate, nadie gano :/"
				movimientos += 1
				ganado = True
			else:
				tirarUsuario(arreglo)
				print chr(27) + "[2J"
				pintarGato(arreglo)
				movimientos += 1
			turnoU = False
		else:
			if movimientos == 9:
				analizarJugada(arreglo)
				print "Empate, nadie gano :/"
				ganado = True
			else:
				tirarMaquina(arreglo)
				print chr(27) + "[2J"
				pintarGato(arreglo)
				movimientos += 1
			turnoU = True


def pintarGato(arreglo):
	for i in arreglo:
		print "\t\t\t\t",
		for j in i:
			print "| ", j, " |",
		print "\n\t\t\t\t", ("-"*23)
	print "\n\n\n"

def tirarMaquina(arreglo):
	sinAsignar = True
	print "La maquina esta pensando..."
	time.sleep(2)
	while sinAsignar:
		numero = random.randint(0,9)
		for i in range(len(arreglo)):
			for j in range(len(arreglo)):
				if numero == arreglo[i][j]:
					arreglo[i][j] = '*'
					analizarJugada(arreglo)
					sinAsignar = False

def tirarUsuario(arreglo):
	valida = False
	while not valida:
		tirada = int(raw_input("Introduce la casilla donde quieres tirar\n"))
		if tirada >= 0 and tirada<=9:
			for i in range(len(arreglo)):
				for j in range(len(arreglo)):
					if tirada == arreglo[i][j]:
						arreglo[i][j] = 'U'
						valida = True
						analizarJugada(arreglo)
			if not valida:
				print "Casilla ocupada :( Trata con otra"
		else:
			print "Introduce una casilla valida"


def analizarJugada(arreglo):
	# Analisis Horizontal
	ganado = False
	for i in range(len(arreglo)):
		ganadasU = 0
		ganadasM = 0
		for j in range(len(arreglo)):
			if arreglo[i][j] == 'U':
				ganadasU += 1
			elif arreglo[i][j] == '*':
				ganadasM += 1

		if ganadasU == 3:
			print "Haz ganado contra la maquina!"
			pintarGato(arreglo)
			sys.exit()

		elif ganadasM == 3:
			print "Haz perdido contra la maquina :("
			pintarGato(arreglo)
			sys.exit()
	# Analisis Horizontal

	# Analisis Vertical
	for i in range(len(arreglo)):
		ganadasU = 0
		ganadasM = 0
		for j in range(len(arreglo)):
			if arreglo[j][i] == 'U':
				ganadasU += 1
			elif arreglo[j][i] == '*':
				ganadasM += 1
		if ganadasU == 3:
			print "Haz ganado contra la maquina!"
			pintarGato(arreglo)
			sys.exit()

		elif ganadasM == 3:
			print "Haz perdido contra la maquina :("
			pintarGato(arreglo)
			sys.exit()
	# Analisis Vertical

	# Analisis diagonal
	if (arreglo[0][0] == '*' and arreglo[1][1] == '*' and arreglo[2][2] == '*') or (arreglo[0][2] == '*' and arreglo[1][1] == '*' and arreglo[2][0] == '*'):
		print "Haz perdido contra la maquina :("
		pintarGato(arreglo)
		sys.exit()

	elif (arreglo[0][0] == 'U' and arreglo[1][1] == 'U' and arreglo[2][2] == 'U') or (arreglo[0][2] == 'U' and arreglo[1][1] == 'U' and arreglo[2][0] == 'U'):
			print "Haz ganado contra la maquina!"
			pintarGato(arreglo)
			sys.exit()

	return ganado
	# Analisis diagonal

jugar(arreglo)